import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class DatabaseSupport {
    static String dbName="reggie";
    static String connectionURL = "jdbc:derby:" + dbName + ";create=true";
    //We keep some original functions in their original class

    //We move create function from three classes- offering, course, and schedule into this class
    //which could help us to use create function for each class more conveniently
    public static Schedule create(int studentId, String name) throws Exception {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            statement.executeUpdate("DELETE FROM schedule WHERE studentId = " + studentId + "");
            return new Schedule(studentId, name);
        }
    }

    public static Course create(String name, int credits) throws Exception {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            statement.executeUpdate("DELETE FROM course WHERE name = '" + name + "'");
            statement.executeUpdate("INSERT INTO course VALUES ('" + name + "', " + credits + ")");
            return new Course(name, credits);
        }
    }

    public static Offering create(Course course, String daysTimesCsv) throws Exception {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();

            ResultSet result = statement.executeQuery("SELECT MAX(id) FROM offering");
            result.next();
            int newId = 1 + result.getInt(1);

            statement.executeUpdate("INSERT INTO offering VALUES (" + newId + ",'"
                    + course.getName() + "','" + daysTimesCsv + "')");
            return new Offering(newId, course, daysTimesCsv);
        }
    }

    //We move find function from three classes- offering, course, and schedule into this class
    //which could help us to use find function for each class more conveniently
    public static Offering find(int id) {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM offering WHERE id =" + id
                    + "");
            if (!result.next())
                return null;

            String courseName = result.getString("name");
            Course course = Course.find(courseName);
            String dateTime = result.getString("daysTimes");
            conn.close();

            return new Offering(id, course, dateTime);
        } catch (SQLException ex) {
            try {
                conn.close();
            } catch (SQLException ignored) {
            }
            return null;
        }
    }


    public static Course find(String name) {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM course WHERE name = '"
                    + name + "'");
            if (!result.next())
                return null;

            int credits = result.getInt("Credits");
            return new Course(name, credits);
        } catch (Exception ex) {
            return null;
        }
    }

    public static Schedule find(int studentId, String name) {
        try {
            try (Connection conn = DriverManager.getConnection(connectionURL)) {
                Statement statement = conn.createStatement();
                ResultSet result = statement.executeQuery("SELECT * FROM schedule WHERE studentId= "
                        + studentId + "");

                Schedule schedule = new Schedule(studentId, name);

                while (result.next()) {
                    int offeringId = result.getInt("offeringId");
                    Offering offering = Offering.find(offeringId);
                    assert offering != null;
                    schedule.add(offering);
                }
                return schedule;
            }
        } catch (SQLException ex) {
            return null;
        }
    }


    public static Collection<Schedule> allSchedules() throws Exception {
        ArrayList<Schedule> result = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            ResultSet results = statement.executeQuery("SELECT DISTINCT studentId, name FROM schedule ORDER BY name");

            while (results.next())
                result.add(DatabaseSupport.find(results.getInt("studentId"), results.getString("name")));
        }
        return result;
    }

    //We move update function from three classes- offering, course, and schedule into this class
    //which could help us to use update function for each class more conveniently
    public void update(String deleteStatement, String insertStatement) throws Exception {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            statement.executeUpdate(deleteStatement);
            statement.executeUpdate(insertStatement);
        }
    }

    public void update(String deleteStatement, ArrayList<String> insertStatements) throws Exception {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            statement.executeUpdate(deleteStatement);
            for(String insertStatement : insertStatements) {
                statement.executeUpdate(insertStatement);
            }
        }
    }

    public void update(ArrayList<String> deleteStatements, String insertStatement) throws Exception {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            for(String deleteStatement : deleteStatements) {
                statement.executeUpdate(deleteStatement);
            }
            statement.executeUpdate(insertStatement);
        }
    }

    public void update(ArrayList<String> deleteStatements, ArrayList<String> insertStatements) throws Exception {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            for(String deleteStatement : deleteStatements) {
                statement.executeUpdate(deleteStatement);
            }
            for(String insertStatement : insertStatements) {
                statement.executeUpdate(insertStatement);
            }
        }
    }

    public static void deleteAll(String target) throws Exception {
        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            Statement statement = conn.createStatement();
            statement.executeUpdate("DELETE FROM " + target);
        }
    }
    //we found out that the Connection conn is actually essential and irreplaceable. Meanwhile, by using the try method, it does not
    //guarantee to connect back to the server every single time. Therefore, we kept the try method to connect for each method.
    //Also, it would be interesting to see if we use a non-SQL database.
}
