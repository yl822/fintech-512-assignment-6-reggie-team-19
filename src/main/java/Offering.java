public class Offering extends DatabaseSupport {
	private final int id;
	private final Course course;
	private final String daysTimes;


	public void update() throws Exception {
		super.update("DELETE FROM offering WHERE id=" + id + "",
				"INSERT INTO offering VALUES(" + id + ",'" + course.getName() + "','" + daysTimes + "')");
	}

	public Offering(int id, Course course, String daysTimesCsv) {
		this.id = id;
		this.course = course;
		this.daysTimes = daysTimesCsv;
	}

	public int getId() {
		return id;
	}

	public Course getCourse() {
		return course;
	}

	public String getDaysTimes() {
		return daysTimes;
	}

	public String toString() {
		return "Offering " + getId() + ": " + getCourse() + " meeting " + getDaysTimes();
	}
}
