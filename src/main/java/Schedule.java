import java.util.*;

public class Schedule extends DatabaseSupport {
	String name;
	int studentId;
	int credits = 0;
	static final int minCredits = 12;
	static final int maxCredits = 18;
	boolean overloadAuthorized = false;
	ArrayList<Offering> schedule = new ArrayList<>();


	public void update() throws Exception {
		ArrayList<String> insertStatements = new ArrayList<>();
		for (Offering value : schedule) {
			insertStatements.add("INSERT INTO schedule VALUES(" + studentId + ",'" + name + "'," + value.getId() + ")");
		}
		super.update("DELETE FROM schedule WHERE studentId = " + studentId + "",
				insertStatements);
	}

	public Schedule(int studentId, String name) {
		this.studentId = studentId;
		this.name = name;
	}

	public void add(Offering offering) {
		credits += offering.getCourse().getCredits();
		schedule.add(offering);
	}

	public void authorizeOverload(boolean authorized) {
		overloadAuthorized = authorized;
	}

	public List<String> analysis() {
		ArrayList<String> result = new ArrayList<>();

		if (credits < minCredits)
			result.add("Too few credits");

		if (credits > maxCredits && !overloadAuthorized)
			result.add("Too many credits");

		checkDuplicateCourses(result);
		checkOverlap(result);

		return result;
	}

	public void checkDuplicateCourses(ArrayList<String> analysis) {
		HashSet<Course> courses = new HashSet<>();
		for (Offering offering : schedule) {
			Course course = offering.getCourse();
			if (courses.contains(course))
				analysis.add("Same course twice - " + course.getName());
			courses.add(course);
		}
	}

	public void checkOverlap(ArrayList<String> analysis) {
		HashSet<String> times = new HashSet<>();

		for (Offering offering : schedule) {
			String daysTimes = offering.getDaysTimes();
			StringTokenizer tokens = new StringTokenizer(daysTimes, ",");
			while (tokens.hasMoreTokens()) {
				String dayTime = tokens.nextToken();
				if (times.contains(dayTime))
					analysis.add("Course overlap - " + dayTime);
				times.add(dayTime);
			}
		}
	}

	public String toString() {
		return "Schedule " + name + ": " + schedule;
	}
}
