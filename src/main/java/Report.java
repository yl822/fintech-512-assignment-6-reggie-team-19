import java.util.*;

public class Report {
	public Report() {
	}

	Hashtable<Integer, ArrayList<String>> offeringToName = new Hashtable<>();

	public void populateMap() throws Exception {
		Collection<Schedule> schedules = DatabaseSupport.allSchedules();
		for (Schedule schedule : schedules) {
			for (Offering offering : schedule.schedule) {
				populateMapFor(schedule, offering);
			}
		}
	}

	private void populateMapFor(Schedule schedule, Offering offering) {
		ArrayList<String> list = offeringToName.computeIfAbsent(offering.getId(), k -> new ArrayList<>());
		list.add(schedule.name);
	}

	public void writeOffering(StringBuilder buffer, ArrayList<String> list, Offering offering) {
		buffer.append(offering.getCourse().getName() + " " + offering.getDaysTimes() + "\n");

		for (String s : list) {
			buffer.append("\t").append(s).append("\n");
		}
	}

	public void write(StringBuilder buffer) throws Exception {
		populateMap();

		Enumeration<Integer> enumeration = offeringToName.keys();
		while (enumeration.hasMoreElements()) {
			Integer offeringId = enumeration.nextElement();
			ArrayList<String> list = offeringToName.get(offeringId);
			writeOffering(buffer, list, Objects.requireNonNull(Offering.find(offeringId)));
		}

		buffer.append("Number of scheduled offerings: ");
		buffer.append(offeringToName.size());
		buffer.append("\n");
	}
}
