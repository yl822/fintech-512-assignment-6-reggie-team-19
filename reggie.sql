
CREATE  course
  (name VARCHAR(50),
   credits INTEGER);

CREATE TABLE  schedule
  (studentId INTEGER,
   name VARCHAR(100),
   offeringId INTEGER);

CREATE TABLE  offering
  (id INTEGER,
   name VARCHAR(50),
   daysTimes VARCHAR(100));
